// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Plan-50690.h"
#include "2DVector-50690.h"

class Raqueta : public Plano  
{
public:
	Vector2D velocidad;
	Vector2D posicion;

	Raqueta();
	virtual ~Raqueta();

	void Mueve(float t);
};
